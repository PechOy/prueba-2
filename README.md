# INFORMACIÓN #
	* Datos del proyecto
		- PROYECTO: Cursos 
		- PROGRAMADOR: I.M. José Francisco Pech Oy
		- EMPRESA: DaCode

	* Versiones
		- Versión Python: 3.7.3
		- Versión Django: 2.2.0

	* Librerias auxiliares
		- cors-headers, es necesario para poder permitir el corss

## Instalación ##
	1. Descargar e instalar python  3.7.3  -> https://www.python.org/downloads/release/python-373/
		- En el instalador seleccionar:
			+ Habilitar la instalación de pip
			+ Asignar variables globales

	2. Instalar django mediante la consola de windows -> pip install django==2.2

	3. Checar las versiones mediante la consola
		- Ejecuta python -V
		- Ejecuta python 
			+ Ejecuta import django
			+ Ejecuta django.VERSION

	4. Instalar cors-headers con pip -> pip install django-cors-headers

## Correr API ##
	
	1. Ejecutar mediante consola los siguientes comandos:

		- cd .../path/del/usuario/prueba2

		- En la directorio raíz del proyecto tengo una BD de SQLite, si se desea hacer uso de ella solo cambie el nombre del archivo "db - Copy.sqlite3" por "db.sqlite3" y salte el siguiente paso

		- Si se desea utilziar una BD nueva ejecuta los siguientes comandos
			- python manage.py makemigrations
			- python manage.py migrate

		- Para ejecutar la API correr por consola -> python manage.py runserver

## Consumir EndPoints(EP) ##
	
	1. La raíz de los EP es http://127.0.0.0:8000/
	2. La comunicación es JSON en request y en response
	3. Agregar headers:
		- Content-Type:application/json
		- X-Requested-With:XMLHttpRequest

	4. Clases:
		- Curso:	
			Titulo = Texto con tamaño máximo de 120
			Descripcion = Texto con tamaño máximo de 180
			Hijos = Leccion[]
			Disponible = bool
			Aprobado = bool

		- Leccion:
			Titulo = Texto con tamaño máximo de 120
			Descripcion = Texto con tamaño máximo de 180
			OCurso = Entero que representa el id del la clase Curso
			CalificacionAprobatoria = Entero que representa el mínimo aprobatorio
			Hijos = Pregunta[]
			Aprobado = bool
			Disponible = bool
			Calificacion = Entero

		- Pregunta:
			Texto = Texto con tamaño máximo de 200	
			OLeccion = Entero que representa el id del la clase Leccion
			valor = Valor para la lección
			NumeroRespuestas = Entero que representa el número de incisos esperados como respuesta
			self.Hijos = Respuesta[]

		- Respuesta:
			Iniciso = Entero
			Texto = Texto con tamaño máximo de 180
			Correcto = bool
			OPregunta = Entero que representa el id del la clase Pregunta 
			
		- User:	
			Nombre = Texto con tamaño máximo de 120
			Apellido = Texto con tamaño máximo de 180
			Cargo = Entero que representa que tipo de usuario es (Maestro/Estudiante - 1/2). Por default es estudiante.
					Se hizo con entero para poder agregar más tipos
			Password = Texto con tamaño máximo de 20


	5. Listado de EP:

		- NOTA:
			El JSON que se retorna contiene un "Code" y "Descripción", en caso de algún fallo interno de la API Code es igual a 400, 
			si hubo algo incorrecto en la petición o algún mensaje de alerta se tiene un "code" igual a 200, 
			si todo resulta bien "code" es 0. En todos los casos "Description" describe lo que sucedió con la petición.
			En caso de necesitar un objeto al rertorno se tiene el parámetro "object" dentro del json

		- CRUD users:
			+ ".../AddUser/"
				* Petición JSON -> {"nombre":"...","apellido":"...","cargo":0,"password":"xxxxx"}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Objeto User}

			+ ".../UpdateUser/"
				* Petición JSON -> {"nombre":"...","apellido":"...","cargo":0,"password":"xxxxx"}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			+ ".../DeleteUser/'
				* Petición JSON -> {"id_user":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}


		- CRUD CURSOS
			"/AddCurso/" 
				* Petición JSON -> {"id_user":0, "titulo":"...", "descripcion":"..."}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Objeto Curso}

			"DeleteCurso/"
				* Petición JSON -> {"id_user":0, "id_curso":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"UpdateCurso/"
				* Petición JSON -> {"id_user":0, "id_curso":0,"titulo":"...", "descripcion":"..."}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"GetCursos/"
				* Petición JSON -> {}
				* Retorna JSON -> {"Code":0,"Description":"OK",,"object":Array Curso[]}
				* NOTA: Este EP no regresa si fue aprobado o no

		- CRUD LECCION
			"AddLeccion/"
				* Petición JSON -> {"id_user":0,"titulo":"...","descripcion":"...","id_curso":0,"calificacion_aprobatoria":70}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Objeto Leccion}
			
			"UpdateLeccion/"
				* Petición JSON -> {"id_user":0,"id_leccion":0,"titulo":"...","descripcion":"...","id_curso":0,"calificacion_aprobatoria":70}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"DeleteLeccion/"
				* Petición JSON -> {"id_user":0,"id_leccion":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"GetLecciones/"
				* PeticiónJSON -> {"id_curso":0}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Array Lección[]}
				* NOTA: Este EP no regresa si fue aprobado o no

			"ChecarLeccion/"
				* Petición JSON -> {"id_usuario":0,"id_leccion":0,"respuestas":[{"id_pregunta":0,"id_respuestas":[1]},{"id_pregunta":2,"id_respuestas":[1],{...}]}
				* Retorna JSON -> {"Code":0,"Description":"OK"}
				* Nota: Este EP, calcula la suma de las respuestas correctas y las asigna en la calificación de la lección por alumno, al cumplir con todos los cursos se aprueba el curso entero.

		- CRUD PREGUNTAS
			"AddPregunta/"
				* Petición JSON -> {"texto":"...",	"id_leccion":0, "valor":0,	"numero_respuestas":1,"id_usuario":0}
				* Retorna JSON -> {"Code":0,"Description":"OK""object":Objeto pregunta}

			"UpdatePregunta/"
				* Petición JSON -> {"id_user":0,id_leccion":0, "texto":"...",	"valor":0,"numero_respuestas":1,"id_pregunta":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"DeletePregunta/"
				* Petición JSON -> {"id_leccion":0, "id_pregunta":0,"id_usuario":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

		- CRUD RESPUESTAS
			"AddRespuesta/"
				* Petición JSON -> {"id_pregunta":5,"id_usuario":2,"respuestas":[{"iniciso":1, "texto":"if", "correcto":true}, {"iniciso":2, "texto":"else", "correcto":false},{"iniciso":2, "texto":"else if", "correcto":false},{...}]}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"UpdateRespuesta/"
				* Petición JSON -> {"id_usuario":0, "id_respuesta":0,"id_pregunta":0 "iniciso" = 0, "texto":"...", "correcto":true}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

			"DeleteRespuesta/"
				* Petición JSON -> {"id_usuario":0, "id_respuesta":0}
				* Retorna JSON -> {"Code":0,"Description":"OK"}

		- Información de usuario sobre su avance
			"GetCursos/"
				* Petición JSON -> {"id_usuario":0, "id_respuesta":0}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Curso[]}	

			"GetLecciones/"
				* Petición JSON -> {"id_usuario":0,"id_curso":0}
				* Retorna JSON -> {"Code":0,"Description":"OK","object":Leccion[]}