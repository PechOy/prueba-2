# Generated by Django 2.2 on 2020-08-08 20:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cursos', '0006_pregunta_numerorespuestas'),
        ('users', '0003_remove_relacionusuariocursos_leccion'),
    ]

    operations = [
        migrations.CreateModel(
            name='RelacionUsuarioLecciones',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Calificación', models.IntegerField()),
                ('Leccion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cursos.Leccion')),
                ('Usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.User')),
            ],
        ),
    ]
