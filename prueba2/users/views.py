from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt 
from .models import *
from cursos.models import *
from django.db.models import Q, Count

"""
	JSON -> {"nombre":"...","apellido":"....","cargo":1/2,"password":"xxxxx"}
"""
@csrf_exempt
def AddUser(request):
	data_json = {"Code":0,"Description":"OK","object":None}
	try:
		_json = GetJson(request)
		usr = User(Nombre=_json["nombre"],Apellido=_json["apellido"],Cargo=_json["cargo"],Password=_json["password"])
		usr.save()
		data_json["object"] = Serializer.Serialize(usr)
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_user":0}
"""
@csrf_exempt
def DeleteUser(request):
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		usr.delete()
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
	return  usr

"""
	JSON -> {"id_user":0,"nombre":"...","apellido":"....","cargo":1/2,"password":"xxxxx"}
"""
@csrf_exempt
def UpdateUser(request):
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		usr.Nombre=_json["nombre"]
		usr.Apellido=_json["apellido"]
		usr.Cargo=_json["cargo"]
		usr.Password=_json["password"]
		usr.save()
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
	return  usr

"""
	JSON -> {"id_user":0}
"""
@csrf_exempt 
def GetCursos(request):	
	data_json = {"Code":0,"Description":"OK","object":[]}
	try:
		_json = GetJson(request)
		crs = Curso.objects.all().order_by("pk")
		rlc = RelacionUsuarioCursos.objects.filter(Usuario=_json["id_user"])
		ids = [x.Curso.pk for x in rlc]
		[print(x.Curso.pk) for x in rlc]
		if len(rlc) == 0:
			[x.SetDisponible(False,False) for x in crs]
			crs[0].SetDisponible(True,False)
		else:
			for x in crs:
				x.SetDisponible(x.pk in ids or x.pk == rlc[len(rlc) -1].pk +1, x.pk in ids)

		data_json["object"] = [ Serializer.Serialize(x) for x in crs]
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_usuario":0,"id_curso":0}
"""
@csrf_exempt 
def GetLecciones(request):	
	data_json = {"Code":0,"Description":"OK","object":[]}
	try:
		_json = GetJson(request)
		crs = Curso.objects.all().order_by("pk")		
		lcc = Leccion.objects.filter(OCurso=_json["id_curso"])
		[print("uyyyy",Serializer.Serialize(x)) for x in lcc]
		rlc = RelacionUsuarioLecciones.objects.filter(Usuario=User.objects.get(pk=_json["id_usuario"]),Leccion__in=lcc)
		ids = [x.Leccion.pk for x in rlc]
		if len(rlc) == 0:
			[x.SetDisponible(False,False) for x in lcc]
			crs[0].SetDisponible(True,False)
		else:
			for x in lcc:
				lmb = next((c for c in rlc if c.Leccion.pk == x.pk),None) 
				cal = 0
				if lmb != None:
					cal = lmb.Calificación
				x.SetDisponible(x.pk in ids or x.pk == rlc[len(rlc) -1].pk +1, x.pk in ids,cal)

		data_json["object"] = [ Serializer.Serialize(x) for x in lcc]
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

def GetJson(request):
	if request.is_ajax() and request.method == "POST":
		json_str = request.body.decode(encoding='UTF-8')
		json_obj = json.loads(json_str)
		return json_obj
	else:
		return None