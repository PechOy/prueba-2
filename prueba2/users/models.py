from django.db import models
from django.conf import settings
from cursos.models import *
from decimal import Decimal
import json
from datetime import datetime
from django.forms.models import model_to_dict

class User(models.Model):	
	Nombre = models.CharField(max_length = 120)
	Apellido = models.CharField(max_length = 180)
	Cargo = models.IntegerField(default=2)
	Password = models.CharField(max_length=20)
	
	def GetCursosDisponibles(self):
		pass

class RelacionUsuarioLecciones(models.Model):	
	Calificación = models.IntegerField()
	Usuario = models.ForeignKey(User,on_delete=models.CASCADE)
	Leccion = models.ForeignKey(Leccion,on_delete=models.CASCADE)
		
class RelacionUsuarioCursos(models.Model):
	Calificación = models.IntegerField()
	Usuario = models.ForeignKey(User,on_delete=models.CASCADE)
	Curso = models.ForeignKey(Curso,on_delete=models.CASCADE)