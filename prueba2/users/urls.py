from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

urlpatterns = [
	path('GetCursos/',views.GetCursos,name='GetCursos'),
	path('GetLecciones/',views.GetLecciones,name='GetLecciones'),

	#CRUD users 
	path('AddUser/',views.AddUser,name='AddUser'),
	path('UpdateUser/',views.UpdateUser,name='UpdateUser'),
	path('DeleteUser/',views.DeleteUser,name='DeleteUser'),
	
]