from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
	path('cursos/', include('cursos.urls')),     
    path('users/', include('users.urls')),   
    #path('admin/', admin.site.urls),
]
