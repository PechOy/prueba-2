from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

urlpatterns = [

	#CRUD CURSOS
	path("AddCurso/",views.AddCurso,name='AddCurso'),
	path('DeleteCurso/', views.DeleteCursos,name='DeleteCursos'),	
	path('UpdateCurso/', views.UpdateCurso,name='UpdateCurso'),
	path('GetCursos/', views.GetCursos,name='GetCursos'),
	

	#CRUD LECCION
	path("AddLeccion/",views.AddLeccion,name='AddLeccion'),
	path("UpdateLeccion/",views.UpdateLeccion,name='UpdateLeccion'),
	path("DeleteLeccion/",views.DeleteLeccion,name='DeleteLeccion'),	
	path('GetLecciones/',views.GetLecciones,name='GetLecciones'),
	path('ChecarLeccion/',views.ChecarLeccion,name='ChecarLeccion'),

	#CRUD PREGUNTAS
	path("AddPregunta/",views.AddPregunta,name='AddPregunta'),
	path("UpdatePregunta/",views.UpdatePregunta,name='UpdatePregunta'),
	path("DeletePregunta/",views.DeletePregunta,name='DeletePregunta'),	

	#CRUD RESPUESTAS
	path("AddRespuesta/",views.AddRespuesta,name='AddRespuesta'),
	path("UpdateRespuesta/",views.UpdateRespuesta,name='UpdateRespuesta'),
	path("DeleteRespuesta/",views.DeleteRespuesta,name='DeleteRespuesta'),

]