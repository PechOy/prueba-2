from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt 
from .models import *
from users.models import *

# CURSOS
"""
	JSON -> {"id_user":0, "titulo":"...", "descripcion":"..."}
"""
@csrf_exempt 
def AddCurso(request):	
	data_json = {"Code":0,"Description":"OK","object":None}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		if usr != None and usr.Cargo == 1:
			crs = Curso(Titulo=_json["titulo"],Descripcion=_json["descripcion"])
			crs.save()		
			data_json["object"] = Serializer.Serialize(crs)
		else:
			data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_user":0, "id_curso":0}
"""	
@csrf_exempt 
def DeleteCursos(request):
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		if usr != None and usr.Cargo == 1:
			crs = Curso.objects.get(pk=_json["id_curso"])
			crs.delete()		
		else:
			data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_user":0, "id_curso":0,"titulo":"...", "descripcion":"..."}
"""	
@csrf_exempt 
def UpdateCurso(request):
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		if usr != None and usr.Cargo == 1:
			crs = Curso.objects.get(pk=_json["id_curso"])
			crs.Titulo = _json["titulo"]		
			crs.Descripcion = _json["descripcion"]
			crs.save()
		else:
			data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

@csrf_exempt 
def GetCursos(request):
	data_json = {"Code":0,"Description":"OK","object":[]}
	try:
		crs = Curso.objects.all()
		[ x.GetLecciones() for x in crs]
		data_json["object"] = [ Serializer.Serialize(x) for x in crs]
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")



#LECCIONES
"""
	JSON -> {"id_user":0,"titulo":"...","descripcion":"...","id_curso":0,"calificacion_aprobatoria":70}
"""
@csrf_exempt 
def AddLeccion(request):	
	data_json = {"Code":0,"Description":"OK","object":None}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		crs = Curso.objects.get(pk=_json["id_curso"])
		if crs != None:
			if usr != None and usr.Cargo == 1:
				lcc = Leccion(Titulo=_json["titulo"],Descripcion=_json["descripcion"],OCurso=crs,CalificacionAprobatoria=_json["calificacion_aprobatoria"])
				lcc.save()		
				data_json["object"] = Serializer.Serialize(lcc)
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_user":0,"id_leccion":0,"titulo":"...","descripcion":"...","id_curso":0,"calificacion_aprobatoria":70}
"""
@csrf_exempt 
def UpdateLeccion(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		crs = Curso.objects.get(pk=_json["id_curso"])
		if crs != None:
			if usr != None and usr.Cargo == 1:
				lcc = Leccion.objects.get(pk=_json["id_leccion"])
				lcc.Titulo=_json["titulo"]
				lcc.Descripcion=_json["descripcion"]
				lcc.CalificacionAprobatoria=_json["calificacion_aprobatoria"]
				lcc.save()						
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_user":0,"id_leccion":0}
"""
@csrf_exempt 
def DeleteLeccion(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		if usr != None and usr.Cargo == 1:
			lcc = Leccion.objects.get(pk=_json["id_leccion"])
			lcc.delete()			
		else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_curso":0}
"""
@csrf_exempt 
def GetLecciones(request):
	_json = GetJson(request)
	data_json = {"Code":0,"Description":"OK","object":[]}
	try:
		lcc = Leccion.objects.filter(OCurso=_json["id_curso"])	
		[ x.GetPreguntas() for x in lcc]
		data_json["object"] = [ Serializer.Serialize(x) for x in lcc]
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

"""
	JSON -> {"id_usuario":0,"id_leccion":0,"respuestas":[{"id_pregunta":0,"id_respuestas":[1]},{"id_pregunta":2,"id_respuestas":[1]}]}
"""
@csrf_exempt 
def ChecarLeccion(request):
	_json = GetJson(request)
	data_json = {"Code":0,"Description":"OK","object":[]}
	try:
		lcc = Leccion.objects.get(pk=_json["id_leccion"])
		prgs = Pregunta.objects.filter(OLeccion=_json["id_leccion"])
		usr = User.objects.get(pk=_json["id_usuario"])
		total = 0
		if len(prgs) == len( _json["respuestas"]):
			for aux in _json["respuestas"]:
				prg = Pregunta.objects.get(pk=aux["id_pregunta"])
				rsp = Respuesta.objects.filter(OPregunta=aux["id_pregunta"],pk__in=aux["id_respuestas"])
				if len(rsp) == prg.NumeroRespuestas:
					chk = [x.Correcto for x in rsp]
					if False not in chk:
						total += prg.valor							
			data_json["object"] = lcc.ReturnCalificacion(total)

			if lcc.Aprobado:
				rlc = RelacionUsuarioLecciones.objects.filter(Usuario=_json["id_usuario"],Leccion=_json["id_leccion"])
				if len(rlc) > 0:
					rlc[0].delete()
				newrlc = RelacionUsuarioLecciones(Usuario=usr,Leccion=lcc,Calificación=total)
				newrlc.save()

				lccs = Leccion.objects.filter(OCurso=lcc.OCurso.pk)
				idsl =[x.pk for x in lccs]
				listaLeccionesTomadas = RelacionUsuarioLecciones.objects.filter(Leccion__in=idsl)

				if len(lccs) == len(listaLeccionesTomadas):
					Ctotal = sum(c.Calificación for c in listaLeccionesTomadas)   

					print("ids",Ctotal)
					rcrs = RelacionUsuarioCursos(Calificación=Ctotal/len(listaLeccionesTomadas),Usuario=usr,Curso=lcc.OCurso)
					rcrs.save()
		else:
			data_json = {"Code":200,"Description":"Faltaron algunas respuestas"}
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")


#PREGUNTA
"""
	JSON -> {"texto":"...",	"id_leccion":0, "valor":0,	"numero_respuestas":1,"id_usuario":0}
"""
@csrf_exempt 
def AddPregunta(request):	
	data_json = {"Code":0,"Description":"OK","object":None}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_usuario"])
		lcc = Leccion.objects.get(pk=_json["id_leccion"])
		if lcc != None:
			if usr != None and usr.Cargo == 1:
				prg = Pregunta(Texto=_json["texto"], OLeccion=lcc,	valor=_json["valor"], NumeroRespuestas=_json["numero_respuestas"])
				prg.save()		
				data_json["object"] = Serializer.Serialize(prg)
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_user":0,id_leccion":0, "texto":"...",	"valor":0,"numero_respuestas":1,"id_pregunta":0}
"""
@csrf_exempt 
def UpdatePregunta(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_user"])
		lcc = Leccion.objects.get(pk=_json["id_leccion"])
		if lcc != None:
			if usr != None and usr.Cargo == 1:
				prg = Pregunta.objects.get(pk=_json["id_pregunta"])
				prg.Texto=_json["texto"]
				prg.valor=_json["valor"]
				prg.NumeroRespuestas=_json["numero_respuestas"]
				prg.save()		
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_leccion":0, "id_pregunta":0,"id_usuario":0}
"""
@csrf_exempt 
def DeletePregunta(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_usuario"])
		lcc = Leccion.objects.get(pk=_json["id_leccion"])
		if lcc != None:
			if usr != None and usr.Cargo == 1:
				prg = Pregunta.objects.get(pk=_json["id_pregunta"])
				prg.delete()
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")

#RESPUESTAS
"""
	JSON -> {"id_pregunta":5,"id_usuario":2,"respuestas":[{"iniciso":1, "texto":"if", "correcto":true}, {"iniciso":2, "texto":"else", "correcto":false},{"iniciso":2, "texto":"else if", "correcto":false}]}
"""
@csrf_exempt 
def AddRespuesta(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_usuario"])
		prg = Pregunta.objects.get(pk=_json["id_pregunta"])
		if prg != None:
			if usr != None and usr.Cargo == 1:
				for x in _json["respuestas"]:
					rsp = Respuesta(Iniciso=x["iniciso"],Texto=x["texto"],Correcto=x["correcto"],OPregunta=prg)
					rsp.save()
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"La pregunta seleccionada no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_usuario":0, "id_respuesta":0,"id_pregunta":0 "iniciso" = 0, "texto":"...", "correcto":true}
"""
@csrf_exempt 
def UpdateRespuesta(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_usuario"])
		prg = Pregunta.objects.get(pk=_json["id_pregunta"])
		if prg != None:
			if usr != None and usr.Cargo == 1:
				rsp = Respuesta.objects.get(pk=_json["id_respuesta"])
				rsp.Iniciso=_json["iniciso"]
				rsp.Texto=_json["texto"]
				rsp.Correcto=_json["correcto"]
				rsp.save()		
			else:
				data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		else:
				data_json = {"Code":200,"Description":"El curso seleccionado no existe"}	
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")
"""
	JSON -> {"id_usuario":0, "id_respuesta":0}
"""
@csrf_exempt 
def DeleteRespuesta(request):	
	data_json = {"Code":0,"Description":"OK"}
	try:
		_json = GetJson(request)
		usr = User.objects.get(pk=_json["id_usuario"])
		if usr != None and usr.Cargo == 1:
			prg = Respuesta.objects.get(pk=_json["id_respuesta"])
			prg.delete()
		else:
			data_json = {"Code":200,"Description":"Este usuario no tiene los permisos suficientes para esta acción"}	
		
	except Exception as e:
		data_json = {"Code":400,"Description":str(e)}
	return HttpResponse(json.dumps(data_json),content_type="application/json")


def GetJson(request):
	if request.is_ajax() and request.method == "POST":
		json_str = request.body.decode(encoding='UTF-8')
		json_obj = json.loads(json_str)
		return json_obj
	else:
		return None