from django.db import models
from django.conf import settings

from decimal import Decimal
import json
from datetime import datetime
from django.forms.models import model_to_dict

class Curso(models.Model):	
	Titulo = models.CharField(max_length = 120)
	Descripcion = models.CharField(max_length = 180)

	def GetLecciones(self):
		self.Hijos = [Serializer.Serialize(aux) for aux in Leccion.objects.filter(OCurso=self.pk)]

	def SetDisponible(self, disponible,aprobado):
		self.Disponible = disponible
		self.Aprobado = aprobado

class Leccion(models.Model):
	Titulo = models.CharField(max_length = 120)
	Descripcion = models.CharField(max_length = 180)
	OCurso = models.ForeignKey(Curso,on_delete=models.CASCADE)
	CalificacionAprobatoria = models.IntegerField(default=70)

	def GetPreguntas(self):
		self.Hijos = []
		for aux in Pregunta.objects.filter(OLeccion=self.pk):
			 aux.GetRespuestas()
			 self.Hijos.append(Serializer.Serialize(aux))

	def ReturnCalificacion(self,calificacion):
		self.Aprobado = calificacion >= self.CalificacionAprobatoria
		aux = Serializer.Serialize(self)
		aux["Calificacion"] = calificacion
		return aux

	def SetDisponible(self, disponible,aprobado,calificacion):
		self.Disponible = disponible
		self.Aprobado = aprobado
		self.Calificacion = calificacion
	
class Pregunta(models.Model):
	Texto = models.CharField(max_length=200)	
	OLeccion = models.ForeignKey(Leccion,on_delete=models.CASCADE)
	valor = models.IntegerField(default=1)
	NumeroRespuestas = models.IntegerField(default=1)

	def GetRespuestas(self, MostrarRespuesta=False):
		self.Hijos = []
		for aux in Respuesta.objects.filter(OPregunta=self.pk):
			if MostrarRespuesta == False:
				aux.Correcto = False
			self.Hijos.append(Serializer.Serialize(aux))
		

class Respuesta(models.Model):
	Iniciso = models.IntegerField()
	Texto = models.CharField(max_length = 280)
	Correcto = models.BooleanField(default=False)
	OPregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)

class Serializer:
	def Serialize(objs):		
		return Serializer.obj_dict(objs)

	def obj_dict(obj):
		if isinstance(obj, Decimal):
			return float(obj)
		elif isinstance(obj, datetime):
			return str(obj.strftime("%Y-%m-%d %H:%m:%S"))
		else:
			aux = model_to_dict(obj)
			if hasattr(obj,"Hijos"):
				aux["Hijos"] = obj.Hijos
			if hasattr(obj,"Disponible"):
				aux["Disponible"] = obj.Disponible
			if hasattr(obj,"Aprobado"):
				aux["Aprobado"] = obj.Aprobado
			if hasattr(obj,"Calificacion"):
				aux["Calificacion"] = obj.Calificacion

			return aux

